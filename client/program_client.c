#include <stdio.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#define MAX 80
#define PORT 8080
#define SA struct sockaddr
char username[1024];

void ClientOP(int sockfd, char *name, char *pass)
{
    char buffer[1024] = {0};
    int uid = getuid();
    char struid[10];
    sprintf(struid, "%d", uid);
    send(sockfd, struid, strlen(struid), 0);
    // uid 0 = SU
    if (uid == 0)
    {
        // we are root
        /* code */

        strcpy(username, "root");
    }
    else
    {
        printf("%s:%s sent\n", name, pass);
        // send
        bzero(buffer, 1024);
        sprintf(buffer, "%s:%s", name, pass);
        send(sockfd, buffer, strlen(buffer), 0);

        strcpy(username, name);

        /* code */
    }

    // read response to username and pass
    bzero(buffer, 1024);
    read(sockfd, buffer, 1024);
    printf("%s", buffer);
    if (strcmp(buffer, "Username or Password Invalid") == 0)
    {
        return 0;
    }
    printf("\ndone\n");

    char query[255];
    //  Start Here
    while (1)
    {

        // send query
        printf("[%s]> ", username);
        // scanf("%[^\n]", query);
        gets(query);
        send(sockfd, query, strlen(query), 0);

        // read response to query
        bzero(buffer, 1024);
        read(sockfd, buffer, 1024);
        printf("%s\n", buffer);
    }
}

int main(int argc, char const *argv[])
{
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
    {
        printf("connected to the server..\n");
    }
    ClientOP(sockfd, argv[2], argv[4]);
}
