#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdbool.h>
#include <dirent.h>
#define MAX 1024
#define PORT 8080
#define SA struct sockaddr

char dir_folderDatabase[] = "databases";
char dir_fileUser[] = "databases/user/user.txt";
int valread;
char msg[1024] = {};
char currDB[1024] = "NULL";
struct user
{
    char username[1024];
    char password[1024];
    int userRoot;
    int rootPerms;
} user;

void Setup()
{
    mkdir("databases", 0777);
    mkdir("databases/user", 0777);
    FILE *fptr;
    fptr = fopen(dir_fileUser, "a");
    fclose(fptr);
}

int Auth(char str[])
{

    FILE *fptr;
    fptr = fopen(dir_fileUser, "r");
    if (fptr == NULL)
    {
        printf("User file not found!");
    }
    char line[1024];
    while (!feof(fptr))
    {
        fscanf(fptr, "%s", line);
        // printf("%s %s %dir\n", line, str, strcmp(origin_str, line));

        if (strcmp(line, str) == 0)
        {
            fclose(fptr);
            // printf("string found!\n");
            return 1;
        }
    }
    // printf("ga ketemu!\n");

    fclose(fptr);
    return 0;
}

int Search_User(char username[])
{

    FILE *fptr;
    fptr = fopen(dir_fileUser, "r");
    if (fptr == NULL)
    {
        printf("User file not found!");
    }
    char line[1024];
    while (!feof(fptr))
    {
        fscanf(fptr, "%s", line);
        printf("%s %s %dir\n", line, username, strstr(line, username));

        if (strstr(line, username))
        {
            fclose(fptr);

            // printf("string found!\n");
            return 1;
        }
    }
    // printf("ga ketemu!\n");

    fclose(fptr);
    return 0;
}

char *Create_User(char str[])
{

    char *ptr;
    if (Search_User(str) == 1)
    {
        printf("Access Denied, user exist\n");
        strcpy(msg, "CREATE USER FAILED, USER EXIST!");
        ptr = msg;
        return ptr;
    }
    char cmd[1024];
    char newname[1024];
    char newpass[1024];
    bzero(newname, 1024);
    bzero(newpass, 1024);
    strcpy(cmd, str);
    char *cmdptr = cmd;
    char *token;
    for (int i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
    {
        if (i == 2)
        {
            strcpy(newname, token);
        }
        else if (i == 5)
        {
            strncpy(newpass, token, strlen(token) - 1);
        }
        else if ((i == 3 && strcmp(token, "IDENTIFIED")) || (i == 4 && strcmp(token, "BY")) || i > 5)
        {
            strcpy(msg, "SYNTAX ERROR!");
        }
    }
    bzero(cmd, 1024);
    printf("%s:%s", newname, newpass);
    sprintf(cmd, "%s:%s", newname, newpass);
    FILE *file = fopen(dir_fileUser, "a");
    fprintf(file, "%s\n", cmd);
    fclose(file);

    strcpy(msg, "CREATE USER SUCCESS!");
    ptr = msg;
    return ptr;
}

char *Grant_Perms(char str[])
{
    char *ptr;
    char msg[1024];
    char dbname[1024];
    char user[1024];
    bzero(dbname, 1024);
    bzero(msg, 1024);
    bzero(user, 1024);

    // full cmd: GRANT PERMISSION [nama_database] INTO [nama_user];

    char parse[1024];
    strcpy(parse, str);
    char *parseptr = parse;
    char *token;

    for (int i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strcpy(dbname, token);
        }
        else if (i == 4)
        {
            strncpy(user, token, strlen(token) - 1);
        }
        else if (i == 3 && strcmp(token, "INTO"))
        {
            strcpy(msg, "SYNTAX ERROR!");
            ptr = msg;
            return ptr;
        }
    }
    if (Search_User(user) == 0)
    {
        strcpy(msg, "USER NOT FOUND!");
        ptr = msg;
        return ptr;
    }
    char namafile[1024];
    sprintf(namafile, "%s/%s/granted_user.txt", dir_folderDatabase, dbname);
    FILE *perm_file;
    perm_file = fopen(namafile, "a");
    if (!perm_file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    fprintf(perm_file, "%s\n", user);
    fclose(perm_file);
    strcpy(msg, "DATABASE ACCESS GRANTED");
    ptr = msg;
    return ptr;
}

char *Use(char str[])
{
    char *ptr;
    char msg[1024];
    bzero(msg, 1024);

    // database
    char *dbptr = str + 4;
    char dbname[1024];
    bzero(dbname, 1024);
    // test
    strncpy(dbname, dbptr, strlen(dbptr) - 1);

    char dir_permissionFile[1024];
    bzero(dir_permissionFile, 1024);
    sprintf(dir_permissionFile, "%s/%s/granted_user.txt", dir_folderDatabase, dbname);

    FILE *perm_file;
    perm_file = fopen(dir_permissionFile, "r");
    if (!perm_file)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    printf("%s\n", user.username);

    char line[1024];
    while (!feof(perm_file))
    {
        fscanf(perm_file, "%s", line);
        printf("read: %s username: %s\n", line, user.username);

        if (strstr(line, user.username))
        {
            fclose(perm_file);
            strcpy(currDB, dbname);
            // printf("string found!\n");
            strcpy(msg, "DATABASE ACCESS GRANTED");
            ptr = msg;
            return ptr;
        }
    }
    fclose(perm_file);
    strcpy(msg, "DATABASE ACCESS DENIED");
    ptr = msg;
    return ptr;
}

char *CreateDB(char str[])
{

    printf("adasdsad");

    char *ptr;
    char msg[1024];

    char dbname[1024];
    bzero(dbname, 1024);

    char parse[1024];
    strcpy(parse, str);
    char *parseptr = parse;
    char *token;

    // CREATE DATABASE [nama_database];
    for (int i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strncpy(dbname, token, strlen(token) - 1);
        }
    }

    char databasepath[1024];
    sprintf(databasepath, "%s/%s", dir_folderDatabase, dbname);
    char *dbpath = databasepath;

    if (mkdir(dbpath, 0777) != 0)
    {
        strcpy(msg, "DATABASE CREATION FAILED!");
        ptr = msg;
        return ptr;
    }

    char permissionfile[1024];
    printf("%s", permissionfile);
    strcpy(permissionfile, databasepath);
    strcat(permissionfile, "/granted_user.txt");
    FILE *perm_file;
    perm_file = fopen(permissionfile, "a");
    if (!perm_file || perm_file == NULL)
    {
        strcpy(msg, "DATABASE NOT FOUND");
        ptr = msg;
        return ptr;
    }
    fprintf(perm_file, "%s\n", user.username);
    fclose(perm_file);

    strcpy(msg, "DATABASE CREATED!");
    ptr = msg;
    return ptr;
}

char *CreateTable(char str[])
{
    char *ptr;
    char msg[1024];
    char fullcmd[1024];
    strcpy(fullcmd, str);
    char *tablename = strtok(fullcmd + 13, " ");
    if (strcmp("NULL", currDB) == 0)
    {
        strcpy(msg, "NO DATABASE IS ACTIVE, PLEASE SELECT ONE!");
        ptr = msg;
        return ptr;
    }

    char tablepath[1024];
    sprintf(tablepath, "%s/%s/%s.tsv", dir_folderDatabase, currDB, tablename);
    printf("%s\n", tablepath);

    char cmd[1024], cmd2[1024];
    strcpy(cmd, str);
    char *cmdptr;
    cmdptr = strchr(cmd, '(') + 1;
    printf("cmdptr %s\n", cmdptr);

    strncpy(cmd2, cmdptr, strlen(cmdptr) - 2);
    printf("cmd: %s\n", cmd2);
    char *argptr = cmd2;
    char *token;

    char *token2;

    char kolomnama[1024], kolomdata[1024];
    FILE *table;
    table = fopen(tablepath, "a");
    for (int i = 0; token = strtok_r(argptr, ",", &argptr); i++)
    {

        printf("%dir %s\n", i, token);
        char kolom[1024];
        strcpy(kolom, token);
        char *kolomptr = kolom;
        for (int j = 0; token2 = strtok_r(kolomptr, " ", &kolomptr); j++)
        {
            if (j == 0)
            {
                strcpy(kolomnama, token2);
            }
            if (j == 1)
            {
                strcpy(kolomdata, token2);
            }
        }
        fprintf(table, "%s:%s", kolomnama, kolomdata);
        fprintf(table, "\t");
    }
    fprintf(table, "\n");

    fclose(table);
    strcpy(msg, "TABLE CREATED!");
    ptr = msg;
    return ptr;
}

int Remove_Col(char columname[], char dirpath[], char temppath[], char filename[])
{

    FILE *fptr, *fptr2;
    fptr = fopen(dirpath, "r");
    fptr2 = fopen(temppath, "w");
    if (fptr == NULL)
    {
        printf("TSV file not found!");
    }
    char line[1024];
    int colcount = 0;
    while (!feof(fptr))
    {
        fscanf(fptr, " %[^\n]", line);

        if (strstr(line, columname))
        {
            printf("lien found %s\n", line);

            char parse[1024];
            strcpy(parse, line);
            char *parseptr = parse;
            char *token;
            for (int i = 0; token = strtok_r(parseptr, "\t", &parseptr); i++)
            {
                printf("%s\n", columname);

                if (strstr(token, columname))
                {
                    colcount = i;
                    continue;
                }

                fprintf(fptr2, "%s\t", token);
            }
            fprintf(fptr2, "\n");
        }
    }

    // printf("ga ketemu!\n");

    fclose(fptr);
    fclose(fptr2);
    remove(dirpath);
    printf("%s %s\n", temppath, dirpath);
    rename(temppath, dirpath);
    // if ( == 0)
    // {
    //     printf("File renamed successfully.\n");
    // }
    // else
    // {
    //     printf("Unable to rename files. Please check files exist and you have permissions to modify files.\n");
    // }
    return 0;
}

int remove_directory(const char *dirpath)
{
    DIR *dir = opendir(dirpath);
    size_t path_len = strlen(dirpath);
    int r = -1;

    if (dir)
    {
        struct dirent *pdirent;

        r = 0;
        while (!r && (pdirent = readdir(dir)))
        {
            int r2 = -1;
            char *buffer;
            size_t len;
            // skip . and ..
            if (!strcmp(pdirent->d_name, ".") || !strcmp(pdirent->d_name, ".."))
            {
                continue;
            }

            len = path_len + strlen(pdirent->d_name) + 2;
            // malloc a new path
            buffer = malloc(len);

            if (buffer)
            {
                struct stat statbuf;

                snprintf(buffer, len, "%s/%s", dirpath, pdirent->d_name);
                if (!stat(buffer, &statbuf))
                {
                    if (S_ISDIR(statbuf.st_mode))
                        r2 = remove_directory(buffer);
                    else
                        r2 = unlink(buffer);
                }
                free(buffer);
            }
            r = r2;
        }
        closedir(dir);
    }

    if (!r)
        r = rmdir(dirpath);

    return r;
}

char *Drop(char str[])
{
    char *ptr;
    char msg[1024];
    char fullcmd[1024];
    char fullcmd2[1024];

    strcpy(fullcmd, str);
    strcpy(fullcmd2, str);

    char *droptype = strtok(fullcmd + 5, " ");
    printf("DROPPING %s\n", droptype);
    char dirpath[1024];
    if (strstr(droptype, "DATABASE"))
    {
        /* code */
        char *token = strtok(fullcmd + 14, " ");
        //
        char DBname[1024];
        strncpy(DBname, token, strlen(token) - 1);

        printf("dropping database %s\n", DBname);
        sprintf(dirpath, "%s/%s", dir_folderDatabase, DBname);
        printf("dirpath %s\n", dirpath);
        remove_directory(dirpath);
        // delete here
    }
    else if (strstr(droptype, "TABLE"))
    {
        /* code */
        if (strcmp(currDB, "NULL") == 0)
        {
            strcpy(msg, "CURRENT DB IS NOT SET!");
            ptr = msg;
            return ptr;
        }

        char *token = strtok(fullcmd + 11, " ");
        char tablename[1024];
        strncpy(tablename, token, strlen(token) - 1);
        printf("dropping table %s\n", tablename);

        sprintf(dirpath, "%s/%s/%s", dir_folderDatabase, currDB, tablename);
        printf("dirpath %s\n", dirpath);
        remove_directory(dirpath);
    }
    else if (strstr(droptype, "COLUMN"))
    {
        /* code */
        if (strcmp(currDB, "NULL") == 0)
        {
            strcpy(msg, "CURRENT DB IS NOT SET!");
            ptr = msg;
            return ptr;
        }
        printf("dropping column\n", droptype);

        char parse[1024], namakolom[1024], namatabel[1024];
        strcpy(parse, fullcmd2);
        printf("%s\n", parse);

        char *parseptr = parse;
        char *token;
        for (int i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
        {
            if (i == 2)
            {
                strcpy(namakolom, token);
            }
            else if (i == 4)
            {
                strncpy(namatabel, token, strlen(token) - 1);
            }
            else if (i == 3 && strcmp(token, "FROM"))
            {
                strcpy(msg, "SYNTAX ERROR!");
                ptr = msg;
                return ptr;
            }
        }
        char newpath[1024];
        sprintf(dirpath, "%s/%s/%s.tsv", dir_folderDatabase, currDB, namatabel);
        sprintf(newpath, "%s/%s/temp.tsv", dir_folderDatabase, currDB);
        Remove_Col(namakolom, dirpath, newpath, namatabel);
        // printf("dirpath %s\n", dirpath);

        // delete here
    }
    strcpy(msg, "DROPPED!");
    ptr = msg;
    return ptr;
}

void ServerOP(int sockfd)
{
    char buffer[1024] = {0};
    // read uid
    bzero(buffer, 1024);
    read(sockfd, buffer, 1024);
    printf("%s\n", buffer);
    // root = 0
    // non root = 1000, or others

    if (strcmp(buffer, "0") == 0)
    {
        user.userRoot = 1;
        /* code */
        printf("root logged in\n");

        // user is admin
        // get admin perms
        strcpy(msg, "Root perms accepted, Welcome root user");
        strcpy(user.username, "root");

        send(sockfd, msg, strlen(msg), 0);
    }
    else
    {
        user.userRoot = 0;
        printf("user trying to login\n");

        // read compound
        bzero(buffer, 1024);
        read(sockfd, buffer, 1024);
        printf("%s\n", buffer);
        // printf("%dir\n", Auth(buffer));
        if (Auth(buffer) == 1)
        {
            printf("authorized\n");
            char usernpass[1024];
            strcpy(usernpass, buffer);
            char *ptr = usernpass;
            char *token;
            for (int i = 0; token = strtok_r(ptr, ":", &ptr); i++)
            {
                if (i == 0)
                {
                    strcpy(user.username, token);
                }
                else if (i == 1)
                {
                    strcpy(user.password, token);
                }
                else
                {
                    break;
                }
            }
            bzero(msg, 1024);

            strcpy(msg, "Accepted, Welcome ");
            strcat(msg, user.username);
        }
        else
        {
            bzero(msg, 1024);

            strcpy(msg, "Username or Password Invalid");
            /* code */
        }
        send(sockfd, msg, strlen(msg), 0);
    }
    while (1)
    {
        bzero(buffer, 1024);
        bzero(msg, 1024);
        read(sockfd, buffer, 1024);

        if (strstr(buffer, "CREATE USER"))
        {
            if (user.userRoot)
            {
                strcpy(msg, Create_User(buffer));
            }
            else
            {
                strcpy(msg, "COMMAND DENIED!");
            }
        }
        else if (strstr(buffer, "USE"))
        {

            strcpy(msg, Use(buffer));
        }
        else if (strstr(buffer, "GRANT PERMISSION"))
        {
            if (user.userRoot)
            {
                strcpy(msg, Grant_Perms(buffer));
            }
            else
            {
                strcpy(msg, "COMMAND DENIED!");
            }
        }
        else if (strstr(buffer, "CREATE DATABASE"))
        {

            strcpy(msg, CreateDB(buffer));
        }
        else if (strstr(buffer, "CREATE TABLE"))
        {

            strcpy(msg, CreateTable(buffer));
        }
        else if (strstr(buffer, "DROP"))
        {

            strcpy(msg, Drop(buffer));
        }
        else if (strstr(buffer, "CHEAT"))
        {
            user.rootPerms = 1;
            user.userRoot = 1;
            strcpy(msg, "Superuser granted");
        }
        else if (strstr(buffer, "QUIT"))
        {
            strcpy(msg, "Server mau bundir");
            send(sockfd, msg, strlen(msg), 0);
            break;
        }
        else
        {
            strcpy(msg, "INVALID COMMAND!");
        }
        send(sockfd, msg, strlen(msg), 0);
    }
}

int main()
{
    // pid_t pid, sid; // Variabel untuk menyimpan PID

    // pid = fork(); // Menyimpan PID dari Child Process

    // /* Keluar saat fork gagal
    //  * (nilai variabel pid < 0) */
    // if (pid < 0)
    // {
    //     exit(EXIT_FAILURE);
    // }

    // /* Keluar saat fork berhasil
    //  * (nilai variabel pid adalah PID dari child process) */
    // if (pid > 0)
    // {
    //     exit(EXIT_SUCCESS);
    // }

    // umask(0);

    // sid = setsid();
    // if (sid < 0)
    // {
    //     exit(EXIT_FAILURE);
    // }

    // if ((chdir("/")) < 0)
    // {
    //     exit(EXIT_FAILURE);
    // }

    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);
    // while (1)
    // {
    //     // Tulis program kalian di sini

    //     sleep(30);
    // }
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA *)&servaddr, sizeof(servaddr))) != 0)
    {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");

    Setup();

    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0)
    {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    len = sizeof(cli);

    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA *)&cli, &len);
    if (connfd < 0)
    {
        printf("server accept failed...\n");
        exit(0);
    }
    else
    {
        printf("server accept the client...\n");
    }

    ServerOP(connfd);
}
